from setuptools import setup

setup(
    name='catboost',
    version='0.20.2',
    packages=[r'C:\Users\GB5757\Documents\data-intelligence\share-libs\lib'],
    package_dir={'': r'C:\Users\GB5757\Downloads\catboost-0.20.2\catboost-0.20.2'},
    url='https://github.com/catboost/catboost',
    license='',
    author='GB5757',
    author_email='',
    description='CatBoost'
)
